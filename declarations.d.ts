declare module '*.svg' {
  import {SvgProps} from 'react-native-svg';
  const content: React.StatelessComponent<SvgProps>;
  export default content;
}

declare module 'react-native-touchable-scale' {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const content: React.ComponentClass<any>;
  export default content;
}
