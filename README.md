# React Native Typescript Boilerplate

### An opinionated [React Native](https://facebook.github.io/react-native/docs/getting-started) Starter Kit with [React Navigation](https://github.com/react-navigation/react-navigation) + [ESLINT] to build iOS / Android apps using [TypeScript](https://github.com/Microsoft/TypeScript-React-Native-Starter)

The project has been setup based off [RN Getting Started](https://facebook.github.io/react-native/docs/getting-started) and instructions from [Microsoft's Github TypeScript React Native Starter](https://github.com/Microsoft/TypeScript-React-Native-Starter) repo.

This repo supports the latest version of React Native (v0.60+) supported by RNN (at the time of writing).

### Who is this for?

Someone looking to jump start building apps using RN and prefers TS. The base setup has been taken care of, just `yarn install` and get going from respective IDEs.

You might also want to [rename](https://medium.com/the-react-native-log/how-to-rename-a-react-native-app-dafd92161c35) the app for your own use.

> _Disclaimer_: This is an **opinionated** approach to building apps with RN. The project structure is inspired by multiple production apps built by the contributors.

The project uses and encourages to use industry best practices / tools / libraries like RNN, redux, tslint, separation of concern and structure to build a maintainable app.

### Table of Contents

- [Project Structure](#project-structure)
- [Running](#running)
- [Lint](#lint)
- [Unit Tests](#unit-tests)
- [Cheat Sheet](#cheat-sheet)
  - [React Native Navigation](#react-native-navigation)
  - [Styles](#styles)
- [Contributing](#contributing)
- [TODO](#todo)

#### Project Structure

```
/
├── android					        Android Native code
├── ios						          iOS Native Code
├── src
│   └── assets
│       └── fonts
│       └── icons
│       └── images
│   ├── commons					    UI common components
│   ├── constants
│   └── containers
│   ├── navigation				  Router, Navigation
│   └── screens
│       └── SampleScreen
│           └── components
│               └── Component1.tsx
│               └── Component2.tsx
│           └── index.ts
│           └── SampleScreen.tsx
│   └── styles
│       └── buttons.ts
│       └── colors.ts
│       └── index.ts
│       └── spacing.ts
│       └── theme.ts
│       └── typography.ts
│   └── utils
├── __tests__					      Unit Tests
│   ├── presentation
│   └── redux
├── .buckconfig
├── .commitlintrc.yml
├── .editorconfig
├── .eslintignore
├── eslintrc.js				    	ESLint configuration
├── .gitattributes
├── .gitignore
├── prettierrc.js
├── .watchmanconfig
├── app.json
├── babel.config.js
├── declarations.d.ts
├── index.js	    				  Application Entry point
├── metro.config.js
├── package.json
├── react-native.config.js
├── README.md
├── tsconfig.json			      TypeScript Configuration
├── tsconfig.paths.json
└── yarn.lock
```

`src`
Only presentation layer for the app, styles, images, icons are meant to be under this.

#### Running

Make sure node version installed is `>=8.11.x <=9`

```
yarn install
```

#### Launch

###### iOS on Simulator

```
yarn run start:ios
```

or

```
yarn run start:ios --simulator="iPhone 6"
```

###### Android

For android, run the Metro Bundler from the terminal

```
yarn run start:ios
```

and then launch from IDE.

#### Lint

To run tslint on the application:

```
yarn lint
```

To fix most tslint issues automatically

```
yarn lint:fix
```

#### Unit Test

Unit tests are under `__test__` directory at root.

To run unit test on the application:

```
npm run test
```

To find unit test coverage for the application:

```
npm run test:coverage
```

#### Cheat Sheet

##### React Native Navigation

The application launches with a blank splash screen, and then moves to a tabbed based home view. Developers can feel free to add application launch logic to this, like fetch user token, load persist state etc., or skip the splash if not required, or change this setup altogether.

##### Styles

The `styles` folder contains `global` style and `typography` for the application. Styles for each screen has been placed with the screen, as they are going to be used together with the screen.
