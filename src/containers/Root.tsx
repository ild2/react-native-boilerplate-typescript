import React from 'react';
import AppNavigator from '@src/navigation/AppNavigator';
import {ThemeProvider} from 'react-native-elements';
import {Theme} from '@src/styles';

const Root = () => {
  return (
    <ThemeProvider theme={Theme.theme}>
      <AppNavigator />
    </ThemeProvider>
  );
};

export default Root;
