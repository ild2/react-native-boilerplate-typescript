import React from 'react';
import RootContainer from './Root';

const App = () => {
  return <RootContainer />;
};

export default App;
