import {useContext} from 'react';
import {
  NavigationScreenProp,
  NavigationContext,
  NavigationRoute,
} from 'react-navigation';

export function useNavigation<Params>() {
  return useContext(NavigationContext) as NavigationScreenProp<
    NavigationRoute,
    Params
  >;
}
