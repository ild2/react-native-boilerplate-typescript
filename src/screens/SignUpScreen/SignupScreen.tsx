import React from 'react';
import {View, StyleSheet} from 'react-native';
import SignUpForm from './components/SignUpForm';
import SignUpWithSocial from './components/SignUpWithSocial';
import {Typography} from '@src/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    paddingHorizontal: 16,
    paddingTop: 36,
  },
  signUpForm: {
    flex: 5,
    width: '100%',
  },
  signUpWithSocial: {
    flex: 3,
    width: '100%',
  },
});

// interface SignUpScreenProps {}

const SignUpScreen = () => {
  return (
    <View style={styles.container}>
      <SignUpForm style={styles.signUpForm} />
      <SignUpWithSocial style={styles.signUpWithSocial} />
    </View>
  );
};

SignUpScreen.navigationOptions = {
  title: 'Sign Up',
  headerTitleStyle: {
    textTransform: 'uppercase',
    fontSize: Typography.baseFontSize,
  },
  headerStyle: {
    borderBottomWidth: 0,
    shadowColor: 'transparent', // (IOS)
    elevation: 0, // (Android)
  },
};
export default SignUpScreen;
