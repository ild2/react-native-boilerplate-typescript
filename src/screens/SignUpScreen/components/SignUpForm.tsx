import React, {useState} from 'react';
import {View, StyleSheet, StyleProp, ViewStyle} from 'react-native';
import {CheckBox, Button} from 'react-native-elements';
import CustomInput from '@commons/Input';
import CustomButton from '@commons/Button';
import BreakingLine from '@src/commons/BreakingLine';
import ButtonIcon from '@assets/icons/arrowRightPrimary.svg';
import EyeIcon from '@assets/icons/eye.svg';
import EyeOffIcon from '@assets/icons/eyeoff.svg';
import {Typography, Colors} from '@src/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  ruleWrapper: {
    flexDirection: 'row',
    width: '100%',
  },
  checkboxContainer: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    padding: 0,
  },
  checkboxText: {
    fontWeight: '300',
    marginRight: 0,
    fontFamily: Typography.fontFamily.primaryRegular,
  },
  termTitle: {
    color: Colors.secondaryLight,
    fontSize: Typography.baseFontSize,
    fontFamily: Typography.fontFamily.primaryRegular,
    paddingTop: 0,
    paddingBottom: 0,
  },
  termBtn: {
    paddingHorizontal: 0,
  },
  termContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

interface SignUpFormProps {
  style: StyleProp<ViewStyle>;
}

const SignUpForm = (props: SignUpFormProps) => {
  const [showPassword, setShowPassword] = useState(false);
  const [checked, setChecked] = useState(false);
  const _onShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const _onChecked = () => {
    setChecked(!checked);
  };

  const _onOpenBrowser = () => {};

  return (
    <View style={props.style}>
      <View style={styles.container}>
        <CustomInput placeholder="Email address" />
        <BreakingLine />
        <CustomInput placeholder="First name" />
        <BreakingLine />
        <CustomInput placeholder="Last name" />
        <BreakingLine />
        <CustomInput
          secureTextEntry={!showPassword}
          placeholder="Password"
          rightIcon={
            showPassword ? (
              <EyeOffIcon onPress={_onShowPassword} />
            ) : (
              <EyeIcon onPress={_onShowPassword} />
            )
          }
        />
        <BreakingLine />
        <View style={styles.ruleWrapper}>
          <CheckBox
            title="I agree with"
            checked={checked}
            onPress={_onChecked}
            containerStyle={styles.checkboxContainer}
            textStyle={styles.checkboxText}
            fontFamily={Typography.fontFamily.primaryLight}
          />
          <Button
            title="Term and Conditions"
            onPress={_onOpenBrowser}
            titleStyle={styles.termTitle}
            containerStyle={styles.termContainer}
            buttonStyle={styles.termBtn}
            type="clear"
          />
        </View>
        <BreakingLine />
        <CustomButton title="Signup to Doppel" icon={<ButtonIcon />} />
      </View>
    </View>
  );
};

export default SignUpForm;
