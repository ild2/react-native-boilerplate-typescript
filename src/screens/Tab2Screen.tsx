import React from 'react';
import {View, Text, Button} from 'react-native';
import {NavigationParams} from 'react-navigation';
import Routes from '@navigation/Routes';

const Tab2Screen = ({navigation}: NavigationParams) => {
  return (
    <View>
      <Button
        title="Go to Home"
        onPress={() => navigation.navigate(Routes.Home)}
      />
      <Text>Tab2Screen</Text>
    </View>
  );
};

export default Tab2Screen;
