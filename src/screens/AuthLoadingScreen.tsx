import React from 'react';
// import AsyncStorage from '@react-native-community/async-storage';
import {View, ActivityIndicator, StatusBar, StyleSheet} from 'react-native';
// import {STORAGE} from '@constants';
// import NavigationRoutes from '@navigation/Routes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

class AuthLoadingScreen extends React.PureComponent {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(props: any) {
    super(props);
    this._checkAuthentication();
  }

  _checkAuthentication = async () => {
    // const {navigation} = this.props;
    // const userToken = await AsyncStorage.getItem(STORAGE.UserToken);
    // navigation.navigate(
    //   userToken ? NavigationRoutes.App : NavigationRoutes.AuthStack,
    // );
  };

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

export default AuthLoadingScreen;
