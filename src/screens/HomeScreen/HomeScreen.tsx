import React from 'react';
import {NavigationParams} from 'react-navigation';
import {Button} from 'react-native';
import Routes from '@navigation/Routes';

const HomeScreen = (props: NavigationParams) => {
  const {navigate} = props.navigation;
  return (
    <Button title="Go to Tab2Screen" onPress={() => navigate(Routes.Tab2)} />
  );
};

HomeScreen.navigationOptions = {
  title: 'Welcome',
};
export default HomeScreen;
