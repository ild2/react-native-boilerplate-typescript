import React from 'react';
import {View, StyleSheet, StyleProp, ViewStyle} from 'react-native';
import CustomButton from '@commons/Button';
import BreakingLine from '@commons/BreakingLine';
import {Colors, Buttons} from '@src/styles';
import ButtonIcon from '@assets/icons/arrowRightSecondary.svg';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  img: {
    height: 84,
    width: 84,
  },
  title: {
    color: Colors.secondary,
    fontWeight: '500',
  },
  btn: {
    ...Buttons.bordered,
    backgroundColor: 'transparent',
    borderColor: Colors.border,
  },
  iconWrapper: {
    backgroundColor: Colors.secondaryLight,
  },
});

interface SignUpButtonProps {
  style: StyleProp<ViewStyle>;
  onPress?: () => void;
}

const SignUpButton = (props: SignUpButtonProps) => {
  return (
    <View {...props}>
      <View style={styles.container}>
        <BreakingLine />
        <CustomButton
          onPress={props.onPress}
          title="Sign up with Email"
          type="outline"
          titleStyle={styles.title}
          buttonStyle={styles.btn}
          icon={<ButtonIcon />}
        />
      </View>
    </View>
  );
};

export default SignUpButton;
