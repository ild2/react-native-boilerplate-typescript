import React, {useState} from 'react';
import {View, StyleSheet, StyleProp, ViewStyle} from 'react-native';
import {Button, Text} from 'react-native-elements';
import Input from '@commons/Input';
import CustomButton from '@commons/Button';
import BreakingLine from '@commons/BreakingLine';
import {Typography, Colors} from '@src/styles';
import ButtonIcon from '@assets/icons/arrowRightPrimary.svg';
import EyeIcon from '@assets/icons/eye.svg';
import EyeOffIcon from '@assets/icons/eyeoff.svg';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
  },
  forgotPasswordWrapper: {
    paddingTop: 47,
    flexDirection: 'row',
    alignItems: 'center',
  },
  forgotPasswordText: {
    fontFamily: Typography.fontFamily.primaryLight,
    fontSize: Typography.baseFontSize,
  },
  restoreTitle: {
    fontFamily: Typography.fontFamily.primaryLight,
    color: Colors.secondary,
    fontWeight: '300',
  },
  restoreBtn: {
    padding: 0,
    marginLeft: 5,
  },
});

interface LoginFormProps {
  style: StyleProp<ViewStyle>;
  onLogin?: () => void;
}

const LoginForm = (props: LoginFormProps) => {
  const [showPassword, setShowPassword] = useState(false);
  const _onShowPassword = () => {
    setShowPassword(!showPassword);
  };

  return (
    <View style={props.style}>
      <View style={styles.container}>
        <Input placeholder="Email address" />
        <BreakingLine />
        <Input
          secureTextEntry={!showPassword}
          placeholder="Password"
          rightIcon={
            showPassword ? (
              <EyeOffIcon onPress={_onShowPassword} />
            ) : (
              <EyeIcon onPress={_onShowPassword} />
            )
          }
        />
        <BreakingLine />
        <CustomButton
          title="Login to Doppel"
          icon={<ButtonIcon />}
          onPress={props.onLogin}
        />
        <View style={styles.forgotPasswordWrapper}>
          <Text style={[styles.forgotPasswordText]}>Forgot password?</Text>
          <Button
            buttonStyle={styles.restoreBtn}
            title="Restore"
            titleStyle={[styles.restoreTitle]}
            type="clear"
            onPress={() => {}}
          />
        </View>
      </View>
    </View>
  );
};

export default LoginForm;
