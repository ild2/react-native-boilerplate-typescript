import React from 'react';
import {View, Image, StyleSheet, StyleProp, ViewStyle} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {
    height: 84,
    width: 84,
  },
});

interface LogoProps {
  style: StyleProp<ViewStyle>;
}
const Logo = (props: LogoProps) => {
  return (
    <View style={props.style}>
      <View style={styles.container}>
        <Image style={styles.img} source={require('@assets/images/logo.png')} />
      </View>
    </View>
  );
};

export default Logo;
