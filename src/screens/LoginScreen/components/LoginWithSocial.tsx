import React from 'react';
import {View, StyleSheet, ViewStyle, StyleProp} from 'react-native';
import {Button, Text} from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
import {Colors, Buttons, Typography} from '@src/styles';
import BreakingLine from '@commons/BreakingLine';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  heading: {
    fontFamily: Typography.fontFamily.primaryLight,
    fontSize: Typography.baseFontSize,
    opacity: 0.6,
    fontWeight: '300',
  },
  button1: {
    flex: 1,
    marginRight: 5,
  },
  button2: {
    flex: 1,
    marginLeft: 5,
  },
  socialBtn: {
    ...Buttons.baseRounded,
    borderColor: Colors.border,
    borderWidth: 1,
  },
  btnGroup: {
    flexDirection: 'row',
  },
});

interface LoginWithSocialProps {
  style: StyleProp<ViewStyle>;
}
const LoginWithSocial = (props: LoginWithSocialProps) => {
  return (
    <View style={props.style}>
      <View style={styles.container}>
        <Text style={[styles.heading]}>Or Login with</Text>
        <BreakingLine />
        <View style={styles.btnGroup}>
          <Button
            containerStyle={styles.button1}
            buttonStyle={styles.socialBtn}
            type="outline"
            icon={
              <Icon name="logo-facebook" size={24} color={Colors.darkBlue} />
            }
          />
          <Button
            containerStyle={styles.button2}
            buttonStyle={styles.socialBtn}
            type="outline"
            icon={
              <Icon
                name="logo-twitter"
                size={24}
                color={Colors.secondaryMedium}
              />
            }
          />
        </View>
      </View>
    </View>
  );
};

export default LoginWithSocial;
