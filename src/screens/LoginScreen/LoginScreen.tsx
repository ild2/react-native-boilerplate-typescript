import React from 'react';
import {View, StyleSheet} from 'react-native';
// import AsyncStorage from '@react-native-community/async-storage';

import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation';

// import { STORAGE } from '@constants';
import {useNavigation} from '@utils/rnHooks';
import Logo from './components/Logo';
import LoginForm from './components/LoginForm';
import LoginWithSocial from './components/LoginWithSocial';
import SignUpButton from './components/SignUpButton';
import Routes from '@navigation/Routes';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
    paddingTop: 36,
  },
  logo: {
    flex: 3,
  },
  loginForm: {
    flex: 4.5,
    width: '100%',
  },
  loginWithSocial: {
    flex: 2,
    width: '100%',
  },
  signUpButton: {
    flex: 2,
    width: '100%',
  },
});

type Navigation = NavigationScreenProp<NavigationState, NavigationParams>;

interface LoginScreenProps {
  navigation: Navigation;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const LoginScreen = (props: LoginScreenProps) => {
  const navigation = useNavigation<NavigationParams>();

  const _onSignupNavigate = () => {
    navigation.navigate('SignUp');
  };

  const _onLogin = () => {
    navigation.navigate({routeName: Routes.App});
  };

  return (
    <View style={styles.container}>
      <Logo style={styles.logo} />
      <LoginForm style={styles.loginForm} onLogin={_onLogin} />
      <LoginWithSocial style={styles.loginWithSocial} />
      <SignUpButton style={styles.signUpButton} onPress={_onSignupNavigate} />
    </View>
  );
};

LoginScreen.navigationOptions = {
  header: null,
};
export default LoginScreen;
