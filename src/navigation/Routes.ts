const NavigationRoutes = {
  Splash: 'Splash',
  AuthLoading: 'AuthLoading',
  Auth: 'Auth',

  App: 'App',

  Login: 'Login',
  SignUp: 'SignUp',

  Home: 'Home',
  Tab1: 'Tab1',
  Tab2: 'Tab2',
};

export default NavigationRoutes;
