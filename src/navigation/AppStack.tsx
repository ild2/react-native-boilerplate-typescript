import React from 'react';
import {createStackNavigator, createBottomTabNavigator} from 'react-navigation';

import NavigationRoutes from './Routes';
import Tab1Screen from '@screens/Tab1Screen';
import Tab2Screen from '@screens/Tab2Screen';
import HomeScreen from '@screens/HomeScreen';
import HomeInactive from '@assets/icons/tab0.svg';
import HomeActive from '@assets/icons/tab0-active.svg';
import Tab1Inactive from '@assets/icons/tab1.svg';
import Tab1Active from '@assets/icons/tab1-active.svg';
import Tab2Inactive from '@assets/icons/tab2.svg';
import Tab2Active from '@assets/icons/tab2-active.svg';
import {Colors, Typography} from '@src/styles';

const HomeStack = createStackNavigator(
  {
    [NavigationRoutes.Home]: {
      screen: HomeScreen,
    },
  },
  {
    initialRouteName: NavigationRoutes.Home,
    headerMode: 'screen',
    mode: 'modal',
    navigationOptions: {
      gesturesEnabled: false,
    },
  },
);

const Tab1Stack = createStackNavigator(
  {
    [NavigationRoutes.Tab1]: {
      screen: Tab1Screen,
    },
  },
  {
    initialRouteName: NavigationRoutes.Tab1,
    headerMode: 'screen',
    mode: 'modal',
    navigationOptions: {
      gesturesEnabled: false,
    },
  },
);

const Tab2Stack = createStackNavigator(
  {
    [NavigationRoutes.Tab2]: {
      screen: Tab2Screen,
    },
  },
  {
    initialRouteName: NavigationRoutes.Tab2,
    headerMode: 'screen',
    mode: 'modal',
    navigationOptions: {
      gesturesEnabled: false,
    },
  },
);

const AppRootStack = createBottomTabNavigator(
  {
    [NavigationRoutes.Home]: {
      screen: HomeStack,
    },
    [NavigationRoutes.Tab1]: {
      screen: Tab1Stack,
    },
    [NavigationRoutes.Tab2]: {
      screen: Tab2Stack,
    },
  },
  {
    initialRouteName: NavigationRoutes.Home,
    defaultNavigationOptions: ({navigation}) => ({
      // eslint-disable-next-line react/display-name
      tabBarIcon: ({focused}: {focused: boolean}) => {
        const {routeName} = navigation.state;
        switch (routeName) {
          case NavigationRoutes.Home: {
            return focused ? <HomeActive /> : <HomeInactive />;
          }
          case NavigationRoutes.Tab1: {
            return focused ? <Tab1Active /> : <Tab1Inactive />;
          }
          case NavigationRoutes.Tab2: {
            return focused ? <Tab2Active /> : <Tab2Inactive />;
          }
        }

        return null;
      },
    }),
    tabBarOptions: {
      activeTintColor: Colors.primary,
      // inactiveTintColor: 'gray',
      labelStyle: {
        fontFamily: Typography.fontFamily.primaryMedium,
        fontSize: Typography.smallerFontSize,
        fontWeight: '500',
      },
    },
  },
);

export default AppRootStack;
