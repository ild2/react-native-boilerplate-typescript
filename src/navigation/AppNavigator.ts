import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import AuthLoadingScreen from '@screens/AuthLoadingScreen';
import SplashScreen from '@screens/SplashScreen';
import NavigationRoutes from './Routes';
import AppStack from './AppStack';
import AuthStack from './AuthStack';

export default createAppContainer(
  createSwitchNavigator(
    {
      Splash: SplashScreen,
      AuthLoading: AuthLoadingScreen,
      App: AppStack,
      Auth: AuthStack,
    },
    {
      initialRouteName: NavigationRoutes.App,
      // initialRouteName: NavigationRoutes.Splash,
    },
  ),
);
