import {createStackNavigator} from 'react-navigation';
import LoginScreen from '@screens/LoginScreen';
import SignUpScreen from '@screens/SignUpScreen';
import NavigationRoutes from './Routes';
import {Typography, Colors} from '@src/styles';

const AuthStack = createStackNavigator(
  {
    [NavigationRoutes.Login]: {
      screen: LoginScreen,
    },
    [NavigationRoutes.SignUp]: {
      screen: SignUpScreen,
    },
  },
  {
    initialRouteName: NavigationRoutes.Login,
    headerMode: 'screen',
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerBackTitleStyle: {
        fontWeight: '300',
        fontFamily: Typography.fontFamily.primaryLight,
        fontSize: Typography.baseFontSize,
      },
      headerTitleStyle: {
        fontFamily: Typography.fontFamily.primaryRegular,
        fontSize: Typography.baseFontSize,
      },
      headerTintColor: Colors.mainDark,
    },
  },
);

export default AuthStack;
