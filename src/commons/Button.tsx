import React from 'react';
import {
  StyleSheet,
  View,
  ViewStyle,
  StyleProp,
  TextStyle,
  ImageStyle,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {Button, IconProps, ButtonProps} from 'react-native-elements';
import {Typography, Colors, Buttons} from '@styles/index';

interface Styles {
  container: ViewStyle;
  button: ViewStyle;
  title: TextStyle;
  iconWrapper: ViewStyle;
}

const styles = StyleSheet.create<Styles>({
  container: {
    width: '100%',
  },
  button: {
    ...Buttons.baseRounded,
    backgroundColor: Colors.primary,
  },
  title: {
    flex: 1,
    textTransform: 'uppercase',
    fontWeight: '500',
    textAlign: 'left',
    paddingTop: 0,
  },
  iconWrapper: {
    height: 24,
    width: 24,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
    borderRadius: 7,
  },
});

interface CustomButtonProps extends ButtonProps {
  buttonStyle?: StyleProp<ViewStyle>;
  iconWrapperStyle?: StyleProp<ViewStyle>;
  iconStyle?: StyleProp<IconProps> & StyleProp<ImageStyle>;
  iconName?: string | undefined;
  iconColor?: string | undefined;
  iconSize?: number;
  iconComp?: boolean;
}

const CustomButton = (props: CustomButtonProps) => {
  return (
    <Button
      {...props}
      containerStyle={[styles.container, props.containerStyle]}
      buttonStyle={[styles.button, props.buttonStyle]}
      titleStyle={[styles.title, props.titleStyle]}
      icon={
        props.icon && props.iconComp ? (
          <View style={[styles.iconWrapper, props.iconWrapperStyle]}>
            <Icon
              size={props.iconSize}
              name={props.iconName || 'arrowright'}
              color={props.iconColor}
              style={props.iconStyle}
            />
          </View>
        ) : (
          props.icon
        )
      }
    />
  );
};

CustomButton.defaultProps = {
  icon: true,
  iconRight: true,
  iconImage: true,
  iconComp: false,
  iconSize: Typography.middleFontSize,
  iconColor: Colors.primary,
};
export default CustomButton;
