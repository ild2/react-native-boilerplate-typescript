import React, {useState} from 'react';
import {Input, InputProps} from 'react-native-elements';
import {ViewStyle, StyleSheet} from 'react-native';
import {Spacing, Colors} from '@src/styles';

interface Styles {
  container: ViewStyle;
  input: ViewStyle;
  inputContainer: ViewStyle;
  label: ViewStyle;
}

const styles = StyleSheet.create<Styles>({
  container: {
    width: '100%',
    borderWidth: 1,
    borderRadius: Spacing.borderRadiusBase,
    paddingHorizontal: 12,
    paddingVertical: 8,
  },
  inputContainer: {
    borderBottomWidth: 0,
  },
  input: {},
  label: {
    fontWeight: '300',
  },
});

interface CustomInputProps extends InputProps {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  containerStyle?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  label?: any;
}

const CustomInput = (props: CustomInputProps) => {
  const [borderColor, setBorderColor] = useState(Colors.border);
  const [label, setLabel] = useState(undefined);
  const [value, setValue] = useState('');

  const _onFocus = () => {
    setBorderColor(Colors.primary);
    setLabel(props.label || props.placeholder);
  };

  const _onBlur = () => {
    setBorderColor(Colors.border);
    !value && setLabel(undefined);
  };

  const _onChangeText = (text: string) => {
    setValue(text);
  };

  return (
    <Input
      label={label}
      {...props}
      value={value}
      onChangeText={_onChangeText}
      labelStyle={[styles.label, props.labelStyle]}
      onFocus={_onFocus}
      onBlur={_onBlur}
      rightIcon={props.rightIcon}
      inputStyle={[styles.input, props.inputStyle]}
      inputContainerStyle={[styles.inputContainer, props.inputContainerStyle]}
      containerStyle={[styles.container, props.containerStyle, {borderColor}]}
    />
  );
};
export default CustomInput;
