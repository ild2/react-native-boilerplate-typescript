import React from 'react';
import {View, StyleSheet, ViewStyle} from 'react-native';
import {Spacing} from '@src/styles';

interface Styles {
  container: ViewStyle;
}

const styles = StyleSheet.create<Styles>({
  container: {
    width: '100%',
    height: Spacing.base,
    backgroundColor: 'transparent',
  },
});

interface BreakingLineProps {
  style?: ViewStyle;
  height?: number;
}

const BreakingLine = ({style, height}: BreakingLineProps) => {
  return <View style={[styles.container, style, {height}]} />;
};
BreakingLine.defaultProps = {
  height: Spacing.base,
};

export default BreakingLine;
