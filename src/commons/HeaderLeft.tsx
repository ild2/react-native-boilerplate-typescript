import React from 'react';
import {View, StyleSheet, ViewStyle, TextStyle, Text} from 'react-native';
import {Typography, Colors, Spacing} from '@src/styles';

interface Styles {
  container: ViewStyle;
  title: TextStyle;
}

const styles = StyleSheet.create<Styles>({
  container: {
    flex: 1,
    paddingLeft: Spacing.base,
  },
  title: {
    fontFamily: Typography.fontFamily.secondaryRegular,
    fontSize: Typography.largeFontSize,
    textTransform: 'uppercase',
    color: Colors.red,
  },
});

interface HeaderLeftProps {
  title: string;
}

const HeaderLeft = ({title}: HeaderLeftProps) => {
  return (
    <View style={styles.container}>
      <Text style={[styles.title]}>{title}</Text>
    </View>
  );
};

export default HeaderLeft;
