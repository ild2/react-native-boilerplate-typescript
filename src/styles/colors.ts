export const primary = '#FF7373';
export const primaryLight = '#fd929f';
export const success = '#32B644';
export const successLight = '#98daa1';
export const secondary = '#189EFF';
export const secondaryMedium = '#55acee';
export const secondaryLight = '#8bceff';
export const secondaryLighter = '#dcf0ff';
export const error = '#FC253F';
export const errorLight = '#fd929f';
export const pending = '#F5A623';
export const pendingLight = '#fad291';
export const mainDark = '#000000';
export const mainDarkLight = '#7f7f7f';

export const darkBlue = '#3b5998';

export const darkestGray = '#29292c';
export const darkGray = '#3d3e44';
export const darkPurple = '#282b36';
export const mediumGray = '#9b9aa1';
export const lightGray = '#e9e9e9';
export const lightWarmGray = '#f5f5f5';
export const offWhite = '#b7bdc5';
export const tan = '#fcf8f2';
export const red = '#ff5b5b';
export const transparent = 'rgba(0, 0, 0, 0)';
export const white = '#ffffff';

export const border = lightGray;
export const baseText = darkGray;
export const darkText = darkestGray;
export const sectionBackground = lightWarmGray;
export const background = white;
export const selected = red;
export const unselected = lightGray;
