import * as Colors from './colors';

// FontSize
export const extraLargeFontSize = 32;
export const largestFontSize = 24;
export const largerFontSize = 20;
export const buttonFontSize = 18;
export const headerFontSize = 18;
export const largeFontSize = 18;
export const middleFontSize = 16;
export const baseFontSize = 14;
export const smallFontSize = 12;
export const smallerFontSize = 10;

// FontFamily
export const fontFamily = {
  primaryRegular: 'Rubik-Regular',
  primaryMedium: 'Rubik-Medium',
  primaryLight: 'Rubik-Light',
  secondaryRegular: 'PlayfairDisplay-Regular',
};

export const base = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
};

const textBase = {
  color: Colors.darkText,
  fontWeight: 'bold',
};

export const headerText = {
  ...textBase,
  fontSize: headerFontSize,
  lineHeight: 24,
};

export const subHeaderText = {
  ...textBase,
  fontSize: headerFontSize,
  lineHeight: 12,
};

export const titleText = {
  ...textBase,
  fontSize: smallFontSize,
  lineHeight: 19,
};

export const tagLineText = {
  ...textBase,
  fontSize: smallerFontSize,
  lineHeight: 14,
};

export const priceText = {
  ...textBase,
  fontSize: smallerFontSize,
  lineHeight: 17,
};

export const bodyText = {
  color: Colors.baseText,
  fontSize: smallFontSize,
  lineHeight: 21,
};

export const bodyHeadlineText = {
  ...bodyText,
};

export const bodyNormalText = {
  ...bodyText,
};

export const link = {
  color: Colors.red,
  fontWeight: 'bold',
};

// export const descriptionText = {
//   color: Colors.baseText,
//   fontSize: smallFontSize,
// };

// export const screenHeader = {
//   ...base,
//   color: Colors.baseText,
//   fontSize: largeFontSize,
//   fontWeight: 'bold',
// };

// export const screenFooter = {
//   ...base,
//   ...descriptionText,
// };

// export const sectionHeader = {
//   ...base,
//   ...headerText,
// };

// export const count = {
//   ...base,
//   ...descriptionText,
// };
