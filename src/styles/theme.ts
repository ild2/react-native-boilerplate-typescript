import * as Typography from './typography';

export const theme = {
  Input: {
    labelStyle: [
      {
        opacity: 0.8,
        fontSize: Typography.smallFontSize,
        fontFamily: Typography.fontFamily.primaryLight,
      },
    ],
    inputStyle: [
      {
        fontSize: Typography.baseFontSize,
        fontFamily: Typography.fontFamily.primaryLight,
      },
    ],
  },
  Button: {
    titleStyle: [
      {
        fontSize: Typography.baseFontSize,
        fontFamily: Typography.fontFamily.primaryMedium,
      },
    ],
  },
};
