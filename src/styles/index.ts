import * as Buttons from './buttons';
import * as Colors from './colors';
import * as Spacing from './spacing';
import * as Typography from './typography';
import * as Theme from './theme';

export {Theme, Typography, Spacing, Colors, Buttons};
