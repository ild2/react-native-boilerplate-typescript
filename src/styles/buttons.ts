import * as Colors from './colors';
import * as Spacing from './spacing';
import * as Typography from './typography';

export const base = {
  paddingHorizontal: Spacing.base,
  paddingVertical: Spacing.base - 2,
};

export const text = {
  color: Colors.white,
  fontSize: Typography.baseFontSize,
  fontWeight: '500',
};

export const textUnselected = {
  ...text,
  color: Colors.mediumGray,
};

export const small = {
  paddingHorizontal: Spacing.small,
  paddingVertical: Spacing.small + 2,
  width: 75,
};

export const large = {
  paddingHorizontal: Spacing.large,
  paddingVertical: Spacing.large + 4,
};

export const rounded = {
  borderRadius: 12,
};

export const selected = {
  backgroundColor: Colors.selected,
};

export const unselected = {
  backgroundColor: Colors.unselected,
};

export const bordered = {
  borderColor: Colors.border,
  borderWidth: 1,
};

export const baseRounded = {
  ...base,
  ...rounded,
};
